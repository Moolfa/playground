﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    [SerializeField] float expansionSpeed = 0.5f;
    [SerializeField] float expansionMultiplier = 0.1f;
    [SerializeField] float maximumExpansion = 10f;
    Transform spriteMask;
    bool expandMask = false;
    float expansionAmount = 0f;
    float expansionFactor = 0f;


    // Start is called before the first frame update
    void Start()
    {
        spriteMask = transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (expandMask && spriteMask.localScale.x < maximumExpansion)
        {
            expansionFactor += expansionMultiplier * Time.deltaTime;
            expansionAmount = spriteMask.localScale.x + expansionSpeed * Time.deltaTime + expansionFactor;
            spriteMask.localScale = new Vector3(expansionAmount, expansionAmount, 1f);
        }
        else if (!expandMask && spriteMask.localScale.x > 0)
        {
            expansionFactor = expansionFactor <= 0f ? 0f : expansionFactor - expansionMultiplier * Time.deltaTime;
            expansionAmount = Mathf.Lerp(expansionAmount, 0, 0.9f * Time.deltaTime);
            spriteMask.localScale = new Vector3(expansionAmount, expansionAmount, 1f);
        }

        if (!expandMask && spriteMask.localScale.x <= 0)
        {
            spriteMask.localScale = new Vector3(0, 0, 1f);
            expansionFactor = 0f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            expandMask = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            expandMask = false;
        }
    }
}
