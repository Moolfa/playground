﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LVL5Character : Character
{
    float direction = 0f;
    // Start is called before the first frame update
    //protected override void Update()
    //{
    //    base.Update();
    //    if (direction==0f && Input.GetAxis("Horizontal") != 0f)
    //    {
    //        print(Input.GetAxis("Horizontal"));
    //        direction = Input.GetAxis("Horizontal");
    //    }
    //    Move(direction);
    //}
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (direction == 0f && Input.GetAxis("Horizontal") != 0f)
        {
            if (Input.GetAxis("Horizontal") >= 0)
                direction = 1f;
            else
                direction = -1f;
            StartCoroutine(EndGame());
        }
        Move(direction);
    }
    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(15f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
