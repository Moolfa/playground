﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Public fields to set up spike parameters
    public GameObject[] Spikes;  //  prefabs
    [SerializeField] public float MinX = -10f;   // Minimum X position
    [SerializeField] public float MaxX = 10f;    // Maximum X position
    private int _numberOfSpikes; // Number of spikes to spawn
    public float SpikeDistance = 2.5f; // Distance between spikes

    void Start()
    {
        _numberOfSpikes = Random.Range(4,7);// matters when using loop method which is based on number of spikes and legit distance
        Spawn();// do while method spawn spikes based on range of x in this case -10f to 10f 
    }

    private void SpawnSpikes()
    {
        // Start instantiating spikes at MinX
        float currentX = MinX;

        // Iterate through number of spikes
        for (int i = 0; i < _numberOfSpikes; i++)
        {
            // Check if the current position exceeds the maximum X limit
            if (currentX > MaxX)
                break; // Exit

            // Create spawn position
            Vector3 SpawnPos = new Vector3(currentX, 4.89f, transform.position.z);

            // Instantiate a random spike prefab from the array
            Instantiate(Spikes[Random.Range(0, Spikes.Length)], SpawnPos, Quaternion.identity);

            // Increment currentX by SpikeDistance for the next spike
            currentX += SpikeDistance;
        }
    }


    private void Spawn()
    {
        // Start instantiating spikes at MinX
        float currentX = MinX;
        /*
        Instantiate spikes while current x which stands for spike postion is lower 
        than MaxX which is last legit x position for spawning spike 

         */
        do
        {
            Vector3 _spawnPos = new Vector3(currentX, 4.89f, transform.position.z);  // 4.89f is added to make spikes spawn vertically in correct postion.
            Instantiate(Spikes[Random.Range(0, Spikes.Length)], _spawnPos, Quaternion.identity);
            currentX += SpikeDistance;
        }
        while (currentX <= MaxX);

    }


}
