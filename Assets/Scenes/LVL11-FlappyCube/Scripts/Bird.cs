﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : Character
{

    [SerializeField] protected float AutoMoveSpeed = 5f; // Speed of moving player automatically to right direction
    [SerializeField] protected float AutoRotateSpeed = 5f;// Speed of rotational force that is applied to player

    private bool _hasStarted = false; // Flag for checking that the game has started
    private bool _canMove = false;


    // Start is called before the first frame update
    protected override void Start()
    {

        base.Start();
        StartCoroutine(WaitBeforeStart());


    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        // BY pressing space level starts
        if (Input.GetKeyDown(KeyCode.Space) && _canMove)
        {
            _hasStarted = true;
        }

        if (_hasStarted && _canMove) 
        {
        transform.Translate(Vector3.right * AutoMoveSpeed * Time.deltaTime); // Move player to the right direction 
        transform.rotation = Quaternion.Euler(0,0,rigidBody.velocity.y * AutoRotateSpeed); // Apply rotational force to Player 
        }
    }


    private IEnumerator WaitBeforeStart()
    {
        yield return new WaitForSeconds(1f);
        _canMove = true; // Allow movement after 1 second
    }


    //Disable moving 
    public override void Move(float h)
    {
        movmentAllowed = false;
    }

    // Modified Jump() from addForce to AddRelativeForce
    public override void Jump()
    {

        if (_canMove)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0f);
            rigidBody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            state = State.Jumping;
        }

    }


    // Changed groundCounter to boolean to make player to be  able to jump on air (double jumping) 
    public override  void OnCollisionEnter2D(Collision2D collision)
    {
       
            state = State.Idle;
            //groundCounter++;
            groundFlag = true;
        
    }
    public override  void OnCollisionExit2D(Collision2D collision)
    {
       
            //groundCounter--;
             groundFlag = false;
            
        
    }
}
