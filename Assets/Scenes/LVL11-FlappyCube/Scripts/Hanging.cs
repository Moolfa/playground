using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hanging : MonoBehaviour
           

{

    [SerializeField] private float HangingSpeed=5f;
    [SerializeField] private float angle = 20f;

    private Transform Platform;
    private float timer;
    private float cuurentAngle=0;

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime * HangingSpeed;
        float angle = Mathf.Sin(timer) * this.angle;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle + cuurentAngle));

    }



   
}
