using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nest : MonoBehaviour
{
    [SerializeField] private Transform Parent;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(SetParentAfterDelay(collision.transform, Parent.transform));
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(SetParentAfterDelay(collision.transform, null));
        }
    }

    private IEnumerator SetParentAfterDelay(Transform child, Transform newParent)
    {
        yield return new WaitForSeconds(0.1f);
        child.parent = newParent;
    }
}
