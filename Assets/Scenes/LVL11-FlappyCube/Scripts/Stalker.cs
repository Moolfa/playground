﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stalker : MonoBehaviour
{

    public Transform target;// Target which in our caseis character
    public Vector3 DistanceFromTarget = new Vector3(0,0,-10f); // Setting distance between character & camera 
    public float SmoothTime = 0.3f;// Seting following speed higher the number slowest the camera
    private Vector3 _velocity = Vector3.zero; 


    // Update is called once per frame
    void Update()
    {
        // Changed position of camera based on target
        transform.position = Vector3.SmoothDamp(transform.position,target.position + DistanceFromTarget, ref _velocity,SmoothTime);
                  
    }
}
