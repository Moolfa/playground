﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character3D : MonoBehaviour
{
    Rigidbody rigidBody;
    protected bool groundFlag;
    [SerializeField] protected float jumpForce = 1f;
    [SerializeField] protected float moveSurge = 5f;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        rigidBody = transform.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }
    }

    protected virtual void FixedUpdate()
    {
        Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    public virtual void Move(float h, float v)
    {
        rigidBody.velocity = new Vector3(moveSurge * h, rigidBody.velocity.y, moveSurge * v);
    }

    public virtual void Jump()
    {
        if (groundFlag)
        {
            print("jump");
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0f, rigidBody.velocity.y);
            rigidBody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            groundFlag = true;
        }
    }
    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            groundFlag = false;
        }
    }
}
