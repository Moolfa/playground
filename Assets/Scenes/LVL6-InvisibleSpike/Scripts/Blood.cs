﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blood : MonoBehaviour
{
    [SerializeField] Sprite[] splatterSprites;
    [SerializeField] GameObject splatterMask;
    GameObject instatiatedSplatter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Spike")
        {
            ContactPoint2D contact = collision.GetContact(0);
            Vector2 pos = contact.point;
            instatiatedSplatter = Instantiate(splatterMask, pos, Quaternion.identity);
            instatiatedSplatter.transform.Rotate(0f, 0f, Random.Range(0f, 360f));
            instatiatedSplatter.GetComponent<SpriteMask>().sprite = splatterSprites[Random.Range(0, splatterSprites.Length)];
            instatiatedSplatter.transform.parent = collision.transform;
        }
    }
}
