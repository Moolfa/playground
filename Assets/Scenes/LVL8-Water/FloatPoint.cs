﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatPoint : MonoBehaviour
{
    Rigidbody2D rigidbody;
    [SerializeField] float waterSurface;
    [SerializeField] float waterForceSurge = 1f;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = transform.parent.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rigidbody.AddForceAtPosition(new Vector2(0f, (waterSurface - transform.position.y) * waterForceSurge), transform.position);
       
    }
}
