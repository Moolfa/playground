﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LVL9Character : Character
{
    SpringJoint2D charHing;
    Vector3 lastPosition;

    [SerializeField] GameObject[] pin;
    [SerializeField] float webForcePower;
    [SerializeField] float velocityMultiplier = 10f;

    int currentPinIndex = 0;
    int nextPinIndex = 0;
    enum WebState {ReadyToShoot, Lifting, Swing, Releasing}
    WebState webState = WebState.ReadyToShoot;


    private LineRenderer lineRenderer;
    private List<RopeSegment> ropeSegments;
    private float ropeSegLen = 0.2f;
    private int segmentLength = 35;
    private float lineWidth = 0.1f;
    private bool bridgePhase = false;
    private bool hitWall = false;
    private bool changePinTarget = true;
    private float ropeLenght = 100f;


    // Start is called before the first frame update
    protected override void  Start()
    {
        base.Start();
        charHing = transform.GetComponent<SpringJoint2D>();
        pin[nextPinIndex].transform.GetChild(0).gameObject.SetActive(true);
        this.lineRenderer = transform.GetComponent<LineRenderer>();

        //Swing();
        //var x = Input.mousePosition.x;
        //var y = Input.mousePosition.y;
        //Vector3 ropeStartPoint = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0f));
        //print(ropeStartPoint);


    }

    // Update is called once per frame
    protected override void Update()
    {
        if (webState == WebState.Lifting)
        {
            lineRenderer.enabled = true;
            lineRenderer.positionCount = 2;
            lineRenderer.SetPositions(new Vector3[] { transform.position, pin[currentPinIndex].transform.position });
        }
        else if (webState == WebState.Swing)
        {
            DrawRope();
        }
        else if (webState == WebState.ReadyToShoot)
            lineRenderer.enabled = false;
        if (Input.GetButtonDown("Jump"))
        {
            if (webState == WebState.Swing)
            {
                webState = WebState.Releasing;
                Release();
            }
        }

        if (Input.GetButton("Jump"))
        {
            if (webState == WebState.ReadyToShoot || webState == WebState.Lifting)
            {
                if (changePinTarget)
                {
                    currentPinIndex = nextPinIndex;
                }   
                Lift(pin[currentPinIndex]);
                webState = WebState.Lifting;
            }
        }

        if (Input.GetButtonUp("Jump"))
        {
            if (webState == WebState.Lifting)
            {
                Swing();
                webState = WebState.Swing;
            }
            else if (webState == WebState.Releasing)
            {
                webState = WebState.ReadyToShoot;
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            NextPin(+1);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            NextPin(-1);
        }
    }

    protected override void FixedUpdate()
    {
        if (webState == WebState.Swing)
        {
            Simulate();
        }
            
    }

    void Lift(GameObject selectedPin)
    {
        changePinTarget = false;
        var tempVelocityX = rigidBody.velocity.x;
        var tempVelocityY = rigidBody.velocity.y;
        rigidBody.gravityScale = 0f;
        Vector2 forceAngel = new Vector2((selectedPin.transform.position.x - transform.position.x),
            (selectedPin.transform.position.y - transform.position.y));
        //rigidBody._velocity = new Vector2(0f, 0f);
        //print("rop lenght: " + ropeLenght + " forceAngel magnitude: " + forceAngel.magnitude);
        if (forceAngel.magnitude > 0.003f)
        {
            if (forceAngel.magnitude < ropeLenght)
            {
                ropeLenght = forceAngel.magnitude;
            }
            if (forceAngel.magnitude > ropeLenght + 0.005)
            {
            
                if (forceAngel.x >0 && rigidBody.velocity.x < 0)
                {
                    print("here help x "+ tempVelocityX);
                    tempVelocityX = 0;
                }
                else if (forceAngel.x < 0 && rigidBody.velocity.x > 0)
                {
                    print("here help x: "+ tempVelocityX);
                    tempVelocityX = 0;
                }
                if (forceAngel.y <0 && rigidBody.velocity.y > 0)
                {
                    print("here help y " + tempVelocityY);
                    tempVelocityY = 0; 
                }
                else if (forceAngel.y > 0 && rigidBody.velocity.y < 0)
                {
                    print("here help y: "+ tempVelocityY);
                    tempVelocityY = 0;
                }
                rigidBody.velocity = new Vector2(tempVelocityX, tempVelocityY);
                //ropeLenght = forceAngel.magnitude;
            }

        

        //if (forceAngel.magnitude > 0.003f)
        //{
            //if (rigidBody._velocity.magnitude < 7f)
            //{
            //    rigidBody.AddForce(webForcePower * forceAngel.normalized);
            //}
            rigidBody.AddForce(webForcePower * forceAngel.normalized * Time.deltaTime);
            
            //rigidBody._velocity = webForcePower * forceAngel;
        //}
        }
        else
        {
            rigidBody.velocity = new Vector2(0f, 0f);
        }
        
    }

    void Swing()
    {
        //charHing.connectedBody = pin[currentPinIndex].GetComponent<Rigidbody2D>();
        //charHing.enabled = true;
        rigidBody.velocity = new Vector2(0f, 0f);
        ropeSegments = new List<RopeSegment>();
        Vector3 ropeStartPoint = pin[currentPinIndex].transform.position;
        segmentLength = (int)((pin[currentPinIndex].transform.position - transform.position).magnitude / ropeSegLen);
        if (segmentLength < 2)
        {
            segmentLength = 2;
        }
        float xDist = (pin[currentPinIndex].transform.position.x - transform.position.x)/segmentLength;
        float yDist = (pin[currentPinIndex].transform.position.y - transform.position.y)/segmentLength;
        
        for (int i = 0; i < segmentLength; i++)
        {
            this.ropeSegments.Add(new RopeSegment(ropeStartPoint));
            ropeStartPoint.y -= yDist;
            ropeStartPoint.x -= xDist;
        }
    }

    void Release()
    {
        ropeLenght = 100;
        changePinTarget = true;
        print("RELEASED");
        rigidBody.gravityScale = 1f;
        Vector2 velocity = transform.position - lastPosition;
        print(velocity + " " + transform.position + " " + lastPosition);
        rigidBody.velocity = velocityMultiplier * velocity;
        lineRenderer.enabled = false;
        charHing.enabled = false;
    }

    void NextPin(int i)
    {
        pin[nextPinIndex].transform.GetChild(0).gameObject.SetActive(false);
        nextPinIndex = nextPinIndex + i;
        if (nextPinIndex < 0)
            nextPinIndex = 0;
        if (nextPinIndex >= pin.Length)
            nextPinIndex = pin.Length - 1;
        pin[nextPinIndex].transform.GetChild(0).gameObject.SetActive(true);
    }

    public struct RopeSegment
    {
        public Vector2 posNow;
        public Vector2 posOld;

        public RopeSegment(Vector2 pos)
        {
            this.posNow = pos;
            this.posOld = pos;
        }
    }

    private void Simulate()
    {
        // SIMULATION
        Vector2 forceGravity = new Vector2(0f, -0.9f);

        for (int i = 1; i < this.segmentLength; i++)
        {
            if (hitWall && i >= segmentLength - 2)
            {

            }
            RopeSegment firstSegment = this.ropeSegments[i];
            Vector2 velocity = firstSegment.posNow - firstSegment.posOld;
            firstSegment.posOld = firstSegment.posNow;
            firstSegment.posNow += forceGravity * Time.fixedDeltaTime;
            if (!hitWall || i < segmentLength - 2)
            {
                //firstSegment.posNow += _velocity;
            }
            if (hitWall)
            {
                if (firstSegment.posNow.x < pin[currentPinIndex].transform.position.x)
                {
                    if (velocity.x < 0)
                    {
                        velocity = new Vector2(0f, 0f);
                    }
                }
                else
                {
                    if (velocity.x > 0)
                    {
                        velocity = new Vector2(0f, 0f);
                    }
                }
            }
            firstSegment.posNow += velocity;  
            this.ropeSegments[i] = firstSegment;
        }


        lastPosition = transform.position;

        //CONSTRAINTS
        for (int i = 0; i < 50; i++)
        {
            this.ApplyConstraint();
            if (!bridgePhase)
            {
                transform.position = ropeSegments[segmentLength - 2].posNow;
            }
        }
    }

    private void ApplyConstraint()
    {
        //Constrant to Mouse
        RopeSegment firstSegment = this.ropeSegments[0];
        //var x = Input.mousePosition.x;
        //var y = Input.mousePosition.y;
        //firstSegment.posNow = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 9f));
        firstSegment.posNow = pin[currentPinIndex].transform.position;
        this.ropeSegments[0] = firstSegment;

        if (bridgePhase)
        {
            RopeSegment endSegment = this.ropeSegments[this.ropeSegments.Count - 1];
            endSegment.posNow = transform.position;
            this.ropeSegments[this.ropeSegments.Count - 1] = endSegment;
        }

        for (int i = 0; i < this.segmentLength - 1; i++)
        {
            RopeSegment firstSeg = this.ropeSegments[i];
            RopeSegment secondSeg = this.ropeSegments[i + 1];

            float dist = (firstSeg.posNow - secondSeg.posNow).magnitude;
            float error = Mathf.Abs(dist - this.ropeSegLen);
            Vector2 changeDir = Vector2.zero;

            if (dist > ropeSegLen)
            {
                changeDir = (firstSeg.posNow - secondSeg.posNow).normalized;
            }
            else if (dist < ropeSegLen)
            {
                changeDir = (secondSeg.posNow - firstSeg.posNow).normalized;
            }

            Vector2 changeAmount = changeDir * error;

            if (i == this.segmentLength - 2 && bridgePhase)
            {
                firstSeg.posNow -= changeAmount;
                this.ropeSegments[i] = firstSeg;
            }
            else if (i == 0)
            {
                secondSeg.posNow += changeAmount;
                this.ropeSegments[i + 1] = secondSeg;
            }
            else
            {
                firstSeg.posNow -= changeAmount * 0.5f;
                this.ropeSegments[i] = firstSeg;
                secondSeg.posNow += changeAmount * 0.5f;
                this.ropeSegments[i + 1] = secondSeg;
            }
                
        }
    }

    private void DrawRope()
    {
        float lineWidth = this.lineWidth;
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;

        Vector3[] ropePositions = new Vector3[this.segmentLength];
        for (int i = 0; i < this.segmentLength; i++)
        {
            ropePositions[i] = this.ropeSegments[i].posNow;
        }

        lineRenderer.positionCount = ropePositions.Length;
        lineRenderer.SetPositions(ropePositions);
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        //print("colide");
        if (collision.gameObject.tag == groundTag)
        {
            rigidBody.gravityScale = 1f;
            bridgePhase = true;
            state = State.Idle;
            groundFlag = true;
        }
        if (collision.gameObject.tag == "Wall")
        {

            hitWall = true;
        }
    }
    public override void OnCollisionExit2D(Collision2D collision)
    {
        
        if (webState == WebState.Swing || webState == WebState.Lifting)
        {
            rigidBody.gravityScale = 0f;
        }
        if (collision.gameObject.tag == groundTag)
        {
            //print("uncolide");
            groundFlag = false;
            bridgePhase = false;
        }
        if(collision.gameObject.tag == "Wall")
        {
            hitWall = false;
        }
    }

}
