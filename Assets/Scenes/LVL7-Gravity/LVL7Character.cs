﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LVL7Character : Character
{

    bool isWorldPortrait, changeTheWorldToLandsacape, changeTheWorldToPortrait, gravityOff, jumpedFlag;
    [SerializeField] float RotationSpeed = 2f;
    [SerializeField] Transform world;
    public Quaternion toPortrate = Quaternion.Euler(0, 0, 90);
    public Quaternion toLandScape = Quaternion.Euler(0, 0, 0);
    float rotateProgress = 0f;
    int rotateDirection = 1;
    float diffRotate = 0f;
    float lastRotation = 0f;

    private int groundCounter = 0;
    private int wallCounter = 0;

    protected override void Update()
    {
        base.Update();
        if (jumpedFlag && (rigidBody.velocity.y < 0 || state == State.Idle))
        {
            rigidBody.gravityScale = 0f;
            rotateProgress = 0f;
            gravityOff = true;
            jumpedFlag = false;
            RotateWorld();
        }
        if (changeTheWorldToLandsacape)
        {
            world.rotation = Quaternion.Slerp(toPortrate, toLandScape, CustomLerp());
            transform.rotation = Quaternion.Slerp(transform.rotation, toLandScape, CustomLerp());
            if (world.rotation.z < 0.005f && gravityOff)
            {
                rigidBody.gravityScale = 1f;
                movmentAllowed = true;
                gravityOff = false;
            }
        }
        if (changeTheWorldToPortrait)
        {
            world.rotation = Quaternion.Slerp(toLandScape, toPortrate, CustomLerp());
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), CustomLerp());
            //print(">>>>>>>>   " + world.rotation.z);
            if (world.rotation.z > 0.70f && gravityOff)
            {
                rigidBody.gravityScale = 1f;
                gravityOff = false;
                movmentAllowed = true;

            }
        }
            

    }
    public override void Jump()
    {
        
        if ((isWorldPortrait && wallCounter > 0) || (!isWorldPortrait && groundCounter > 0))
        {
            movmentAllowed = false;
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0f);
            rigidBody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            state = State.Jumping;
            jumpedFlag = true;
        }
    }

    void RotateWorld()
    {
        state = State.falling;
        if (isWorldPortrait)
        {
            changeTheWorldToLandsacape = true;
            changeTheWorldToPortrait = false;
            isWorldPortrait = false;
        }
        else
        {
            changeTheWorldToLandsacape = false;
            changeTheWorldToPortrait = true;
            isWorldPortrait = true;
        }
    }

    float CustomLerp()
    {
        if(rotateProgress < 0.5f)
        {
            rotateProgress = Mathf.Lerp(rotateProgress, 1f, 0.5f * Time.deltaTime);
            diffRotate = (rotateProgress - lastRotation)/Time.deltaTime;
            lastRotation = rotateProgress;

        }
        else if(rotateProgress < 1f)
        {
            rotateProgress += diffRotate* Time.deltaTime;
        }
        else
        {
            rotateProgress = 1;
            changeTheWorldToLandsacape = false;
            changeTheWorldToPortrait = false;
        }
        return rotateProgress;
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            groundCounter++;
        }
        if (collision.gameObject.tag == "Wall")
        {
            wallCounter++;
        }
    }
    public override void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            groundCounter--;
        }
        if (collision.gameObject.tag == "Wall")
        {
            wallCounter--;
        }
    }



}
