﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSlowMotion : Rotate
{
    [SerializeField] GameObject character;

    // Start is called before the first frame update
    void Start()
    {
        
        //MonoBehaviour.print(p1.groundTag); 
    }

    void OnEnable()
    {
        speed = minSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        var g = character.GetComponent<Character>().groundFlag;
        //print(g);
        if (Input.anyKey || g == false)
        {
            speed = maxSpeed;
        }
        else
        {
            speed = minSpeed;
        }
        switch (way)
        {
            case whichWayToRotate.AroundX:
                transform.Rotate(Vector3.right * Time.deltaTime * speed);
                break;
            case whichWayToRotate.AroundY:
                transform.Rotate(Vector3.up * Time.deltaTime * speed);
                break;
            case whichWayToRotate.AroundZ:
                transform.Rotate(Vector3.forward * Time.deltaTime * speed);
                break;
        }
    }
}
