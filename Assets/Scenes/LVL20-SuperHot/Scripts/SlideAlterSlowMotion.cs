﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideAlterSlowMotion : SlideAlter
{
    [SerializeField] GameObject character;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnEnable()
    {
        movingTransform = transform.GetChild(0);
        movingTransform.position = transform.position;
        currentSpeed = new Vector3(0f, 0f, 0f);
        switch (way)
        {
            case whichWayToSlide.Horizontal:
                speed = new Vector3(minSpeed, 0f, 0f);
                break;
            case whichWayToSlide.Vertical:
                speed = new Vector3(0f, minSpeed, 0f);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (slideType == SlideType.SmoothChangeDirection)
        {
            if ((movingTransform.localPosition.x > bounds || movingTransform.localPosition.y > bounds) && moveDirection == 1)
            {
                currentSpeed = Vector3.Lerp(currentSpeed, new Vector3(0f, 0f, 0f), dampingFactor * Time.deltaTime);
            }
            else if ((movingTransform.localPosition.x < bounds * -1 || movingTransform.localPosition.y < bounds * -1) && moveDirection == -1)
            {
                currentSpeed = Vector3.Lerp(currentSpeed, new Vector3(0f, 0f, 0f), dampingFactor * Time.deltaTime);
            }
            else
            {
                currentSpeed = Vector3.Lerp(currentSpeed, speed * moveDirection, dampingFactor * Time.deltaTime);
            }
            if (Mathf.Abs(currentSpeed.x) + Mathf.Abs(currentSpeed.y) < 0.2)
            {
                moveDirection = moveDirection * -1;
                currentSpeed = Vector3.Lerp(currentSpeed, speed * moveDirection, dampingFactor * Time.deltaTime);
            }
            movingTransform.Translate(currentSpeed * Time.deltaTime);
        }
        else
        {
            if (moveHorizontaly)
            {
                MoveHorizontally(LeftMargin, RightMargin);
            }
            if (moveVerticaly)
            {
                MoveVertically(minHeight, maxHeight);
            }
        }
    }
    override protected void  MoveHorizontally(float minPosition, float maxPosition)
    {
        if (movingTransform.localPosition.x > maxPosition)
            horizontalSign = -1;
        if (movingTransform.localPosition.x < minPosition)
            horizontalSign = 1;
        var g = character.GetComponent<Character>().groundFlag;
        if (Input.anyKey || g == false)
        {
            sliderSpeedH = maxSpeed;
        }
        else
        {
            sliderSpeedH = minSpeed;
        }
        movingTransform.Translate(new Vector2(sliderSpeedH * horizontalSign * Time.deltaTime, 0));
    }
    override protected void MoveVertically(float minHeight, float maxHeight)
    {
        if (movingTransform.localPosition.y > maxHeight)
            verticalSign = -1;
        if (movingTransform.localPosition.y < minHeight)
            verticalSign = 1;
        if (Input.anyKey)
        {
            sliderSpeedV = maxSpeed;
        }
        else
        {
            sliderSpeedV = minSpeed;
        }
        movingTransform.Translate(new Vector2(0, sliderSpeedV * verticalSign * Time.deltaTime));
    }
}

