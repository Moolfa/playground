using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class Shooting : MonoBehaviour
{



    private enum PartStates {Aiming,InAir,Hit };

    private PartStates currentState = PartStates.Aiming;



    [SerializeField] private GameObject[] Parts;
    [SerializeField] private float  MaxLaunchForce = 20f;
    [SerializeField] private int predictionSteps = 30;
    [SerializeField] private float intervals = 0.1f;
    [SerializeField] private LayerMask GroundLayer;
    [SerializeField] private float RestartDelay = 10f;
    [SerializeField] private float aimSpeed = 1f;


    [SerializeField] private Vector3 CameraOffset =  new Vector3(0f, 0f, -10f);


     private Camera mainCamera;


    //[SerializeField] private float DrawTrajectorySpeed = 1f;

    //private float t = 0.01f;



    private HandlerScript hs;
    private bool gameOverStarted = false;
    private bool gameWon = false;
    private LineRenderer lineRenderer;
    private int currentPartIndex = 0;
    private float currentLaunchForce = 0f;
    private Vector2 launchDirection = Vector2.right;

    private bool _canShoot = true;

    private Vector3 _cameraVelocity = Vector3.zero;
    private Vector3 originalCameraPosition;



    private void Awake()
    {

        mainCamera = Camera.main;


    }


    // Start is called before the first frame update
    void Start()
    {

        
        hs = GameObject.FindGameObjectWithTag("TextHandler").GetComponent<HandlerScript>();

        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.positionCount = predictionSteps;        

    }

    // Update is called once per frame
    void Update()
    {


        if (currentPartIndex < Parts.Length && currentState==PartStates.Aiming && _canShoot)
        {
            DrawTrajectory();
            lineRenderer.enabled = true;
        }
        else
        {
            lineRenderer.enabled = false;
        }

        switch (currentState)
        {
            case PartStates.Aiming:
                HandleAiming();
                RotatePartWhileAiming();

                if (Input.GetKeyDown(KeyCode.Space) && _canShoot) { 

                currentLaunchForce = 5f;

                } else if (Input.GetKey(KeyCode.Space) && _canShoot)
                {

                    currentLaunchForce = Mathf.Min(currentLaunchForce + Time.deltaTime * 10f, MaxLaunchForce);
                }
                else if (Input.GetKeyUp(KeyCode.Space) && _canShoot && currentPartIndex < Parts.Length)
                {
                    PrepareToLaunch();
                }
             break;

            case PartStates.InAir:
                if (Parts[currentPartIndex].GetComponent<Rigidbody2D>().velocity==Vector2.zero)
                {
                    currentState = PartStates.Hit;
                }
            break;

            case PartStates.Hit:
                ProcessHitState();
                break;

        }


        if (Input.GetKey(KeyCode.S))
        {
            CancelAiming();
        }


    }

    private void CancelAiming()
    {
        _canShoot = false;
        launchDirection = Vector2.zero;
        currentLaunchForce = 0f;
        float _rRotation = Parts[currentPartIndex].transform.rotation.z;
        Parts[currentPartIndex].transform.rotation = Quaternion.identity;
        StartCoroutine(EnableShootingAfterOneSecond());
    }

    private void ProcessHitState()
    {
        // Reset speed after every launch
        launchDirection = Vector2.right;
        Parts[currentPartIndex].transform.rotation = Quaternion.identity;
        currentLaunchForce = 0f;
        currentState = PartStates.Aiming;
        currentPartIndex++;

        if (currentPartIndex >= Parts.Length && !gameOverStarted)
        {
            StartCoroutine(WaitAndCheckCompletion());
            gameOverStarted = true;
        }
    }

    private void PrepareToLaunch()
    {
        // Set up for launching
        LaunchParts();
        currentState = PartStates.InAir;
        StartCoroutine(CameraFollowCoroutine(Parts[currentPartIndex].transform));
    }

    private void RotatePartWhileAiming()
    {
        if (currentPartIndex >= Parts.Length) return;
        float _shootAngle = Mathf.Atan2(launchDirection.y, launchDirection.x) * Mathf.Rad2Deg;
        Parts[currentPartIndex].transform.rotation = Quaternion.Euler(0, 0, _shootAngle);
    }

    private IEnumerator EnableShootingAfterOneSecond()
    {
        
        yield return new WaitForSeconds(1f);
        _canShoot = true; 
    } 

    private void HandleAiming()
    {

       

        float angleChange = aimSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.A)) launchDirection = Quaternion.Euler(0,0,angleChange) * launchDirection;
        if (Input.GetKey(KeyCode.D)) launchDirection = Quaternion.Euler(0, 0, -angleChange) * launchDirection;


        float angle = Vector2.SignedAngle(Vector2.right,launchDirection);
        angle = Mathf.Clamp(angle, -40f, 85f);
        launchDirection = Quaternion.Euler(0, 0, angle) * Vector2.right;
    }


    private void LaunchParts()
    {
        // Apply accumulated force using AddForce
        Rigidbody2D rb = Parts[currentPartIndex].GetComponent<Rigidbody2D>();


        if (rb == null) { 
        
            rb = Parts[currentPartIndex].AddComponent<Rigidbody2D>();   

        
        }

        Debug.Log(rb.velocity.x + "," + rb.velocity.y + ",");

        
        if (!Parts[currentPartIndex].TryGetComponent<BoxCollider2D>(out var bc))
        {

            bc = Parts[currentPartIndex].AddComponent<BoxCollider2D>();

        }

        rb.AddForce(launchDirection * currentLaunchForce, ForceMode2D.Impulse);
        hs.UpdateLives();


    }


    private void DrawTrajectory()
    {
        Vector3 startPosition = Parts[currentPartIndex].transform.position;
        Vector3 initialVelocity = launchDirection * currentLaunchForce;
        Vector3 gravity = Physics2D.gravity;

        for (int i = 0; i < predictionSteps; i++)
        {
            float t = i * intervals;
            Vector3 position = startPosition + initialVelocity * t + 0.5f * gravity * t * t;

            if (i > 0)
            {
                Vector3 previousPosition = lineRenderer.GetPosition(i - 1);
                RaycastHit2D hit = Physics2D.Raycast(previousPosition, (position - previousPosition).normalized, Vector3.Distance(previousPosition, position), GroundLayer);

                if (hit.collider != null)
                {
                    lineRenderer.positionCount = i + 1;
                    lineRenderer.SetPosition(i, hit.point);
                    break;
                }
            }

            if (i < lineRenderer.positionCount)
            {
                lineRenderer.SetPosition(i, position);
            }
        }
    }

    private IEnumerator WaitAndCheckCompletion()
    {
        yield return new WaitForSeconds(RestartDelay);

        if (!gameWon) // Check if the game was won
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("FinishFlag")) // Check for collision with the FinishFlag
        {
            gameWon = true; // Mark the game as won
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // Load the next level
        }
    }



    private IEnumerator CameraFollowCoroutine(Transform part)
    {
        _canShoot = false;
        if (currentState == PartStates.InAir || currentState == PartStates.Hit)
        {

            originalCameraPosition = mainCamera.transform.position;
            float followDuration = 5f; // Adjust duration as needed
            float elapsedTime = 0f;
            Vector3 partDestination;

            while (part != null && elapsedTime < followDuration)
            {
                mainCamera.transform.position = Vector3.SmoothDamp(mainCamera.transform.position,
                    part.position + CameraOffset, ref _cameraVelocity, 0.1f);
                elapsedTime += Time.deltaTime;
                partDestination = part.position;
                yield return null;
            }

            // Return camera to original position after follow duration
            mainCamera.transform.position = originalCameraPosition;
            _canShoot=true;
        }
    }
}
