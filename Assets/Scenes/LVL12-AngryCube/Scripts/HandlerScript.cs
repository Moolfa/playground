using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;



public class HandlerScript : MonoBehaviour
{

    [SerializeField] private Text LivesText;
    private int remaininglives=3;


    [ContextMenu("Decrease cubes number")]
    public void UpdateLives()
    {
        remaininglives -= 1;
        LivesText.text = "Cubes: "+remaininglives.ToString();
    }

}
