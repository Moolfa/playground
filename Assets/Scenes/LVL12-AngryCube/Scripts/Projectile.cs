using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private ParticleSystem ColliderParticlePrefab; // Particle for collision impact
    [SerializeField] private ParticleSystem ShatterParticlePrefab;  // Particle for shattering effect

    private int _hasCollided = 0;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Brick"))
        {
            if (_hasCollided == 0)
            {
                // Instantiate and play collision particle at the point of impact
                ParticleSystem colliderParticle = Instantiate(ColliderParticlePrefab, collision.contacts[0].point, Quaternion.identity);
                colliderParticle.Play();
                Destroy(colliderParticle.gameObject, colliderParticle.main.duration);

                // Set flag to prevent repeated collision handling
                _hasCollided++;

                // Start coroutine to handle delayed shatter effect and destruction
                StartCoroutine(DestroyProjectileAfterDelay());
            }
        }
    }

    private IEnumerator DestroyProjectileAfterDelay()
    {
        // Wait for delay before destruction
        yield return new WaitForSeconds(3f);

        // Instantiate and play shatter particle at the projectile's final position
        ParticleSystem shatterParticle = Instantiate(ShatterParticlePrefab, transform.position, Quaternion.identity);
        shatterParticle.Play();
        Destroy(shatterParticle.gameObject, shatterParticle.main.duration);

        // Destroy the projectile
        Destroy(gameObject);
    }
}
