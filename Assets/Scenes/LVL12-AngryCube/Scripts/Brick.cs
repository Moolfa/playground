using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{

    private float delay = 0.5f;
    private ParticleSystem _particle;
    private ContactPoint2D _hit;
    private BoxCollider2D boxCollider2;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        _particle = GetComponentInChildren<ParticleSystem>();
        boxCollider2 = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Projectile"))
        {
        //_hit = collision.contacts[0];
        //_particle = Instantiate(_particle, _hit.point,Quaternion.identity);
        StartCoroutine(DestroyParticleAfterDelay(_particle,delay));

        }
    }


    IEnumerator DestroyParticleAfterDelay(ParticleSystem particleSystem,float delay)
    {
        particleSystem.Play();
        boxCollider2.enabled = false;
        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(delay);
        Destroy(transform.gameObject);
    }

}
