using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingFlag : FinishFlag
{
    // Start is called before the first frame update

    [SerializeField] private float PeriodSpeed = 2f;
    [SerializeField] private float amplitude = 1f;
    private Vector3 startPosition;

    void Start()
    {

        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {   
        float _yOffset = Mathf.Sin(PeriodSpeed * Time.time) * amplitude;
        transform.position +=new Vector3(0,_yOffset,0);

    }
}
