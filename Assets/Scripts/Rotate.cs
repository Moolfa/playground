﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Rotate : MonoBehaviour
{
	public float minSpeed = 15.0f;
	public float maxSpeed = 50.0f;

	protected private float speed;

	public enum whichWayToRotate { AroundX, AroundY, AroundZ }

	public whichWayToRotate way = whichWayToRotate.AroundX;

	// Use this for initialization
	void Start()
	{

	}

	void OnEnable()
	{
		//print("SPAWN-@@--->>>" + transform.parent.parent.gameObject);
		speed = Random.Range(minSpeed, maxSpeed);
	}

	// Update is called once per frame
	void Update()
	{

		switch (way)
		{
			case whichWayToRotate.AroundX:
				transform.Rotate(Vector3.right * Time.deltaTime * speed);
				break;
			case whichWayToRotate.AroundY:
				transform.Rotate(Vector3.up * Time.deltaTime * speed);
				break;
			case whichWayToRotate.AroundZ:
				transform.Rotate(Vector3.forward * Time.deltaTime * speed);
				break;
		}
	}
}