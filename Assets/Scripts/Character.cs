﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    protected Rigidbody2D rigidBody;
    public bool groundFlag;
    private int groundCounter = 0;
    protected string groundTag = "Ground";
    protected enum State {Idle, Jumping, falling}
    protected State state = State.Idle;
    protected bool movmentAllowed = true;
    protected Rotate rotate;


    [SerializeField] protected float jumpForce = 1f;
    [SerializeField] protected float moveSurge = 5f;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        rigidBody = transform.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (movmentAllowed)
            Move(Input.GetAxis("Horizontal"));
    }

    public virtual void Move(float h)
    {
        rigidBody.velocity = new Vector2(moveSurge * h, rigidBody.velocity.y);
    }

    public virtual void Jump()
    {
        if (groundFlag)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0f);
            rigidBody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            state = State.Jumping;
        }
    }
    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == groundTag)
        {
            state = State.Idle;
            groundCounter++;
            if (groundCounter > 0)
            {
                groundFlag = true;
            }
        }
    }
    public virtual void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == groundTag)
        {
            groundCounter--;
            if(groundCounter <= 0)
            {
                groundFlag = false;
            }
        }
    }
}
