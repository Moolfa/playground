﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slide : MonoBehaviour
{
    [SerializeField] protected float minSpeed = 5.0f;
    [SerializeField] protected float maxSpeed = 8.0f;
    [SerializeField] protected float bounds = 15f;

    public enum whichWayToSlide { Left, Right, Random }
    public whichWayToSlide way = whichWayToSlide.Random;


    private Vector3 speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnEnable()
    {
        speed = new Vector3 (Random.Range(minSpeed, maxSpeed), 0f, 0f);
        switch (way)
        {
            case whichWayToSlide.Left:
                speed = -1 * speed;
                break;
            case whichWayToSlide.Right:
                // Assume min and max speed are both posetive.
                break;
            case whichWayToSlide.Random:
                if (Random.value < 0.5)
                {
                    speed = -1 * speed;
                }
                break;
        } 
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed  * Time.deltaTime);
        if (Mathf.Abs(transform.position.x) > bounds)
        {
            if (speed.x > 0)
            {
                transform.position = new Vector3 (-1 * bounds, transform.position.y, transform.position.z);
            } else
            {
                transform.position = new Vector3(bounds, transform.position.y, transform.position.z);
            }
        }
    }

    
}
