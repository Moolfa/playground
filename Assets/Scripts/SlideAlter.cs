﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideAlter : MonoBehaviour
{
    
    
    public enum SlideType {FixBoundry, SmoothChangeDirection}
    [SerializeField] protected SlideType slideType;
    
    [Tooltip("Options for smooth slider")]
    [Header("Smooth Change Direction")]
    [SerializeField] protected float minSpeed = 5.0f;
    [SerializeField] protected float maxSpeed = 6.0f;
    [SerializeField] protected float bounds = 3f;
    [SerializeField] protected float dampingFactor = 3f;
    [SerializeField] protected enum whichWayToSlide { Horizontal, Vertical }
    [SerializeField] protected whichWayToSlide way = whichWayToSlide.Vertical;

    [Tooltip("Options for Fix Boundry slider")]
    [Header("Fix Boundry")]
    [SerializeField] protected bool moveHorizontaly;
    [SerializeField] protected bool moveVerticaly;
    [SerializeField] protected float sliderSpeedH;
    [SerializeField] protected float LeftMargin;
    [SerializeField] protected float RightMargin;
    [SerializeField] protected float sliderSpeedV;
    [SerializeField] protected float minHeight;
    [SerializeField] protected float maxHeight;


    protected private Transform movingTransform;
    protected private int moveDirection = 1;
    protected private Vector3 speed;
    protected private Vector3 currentSpeed;

    protected private int verticalSign = 1;
    protected private int horizontalSign = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    void OnEnable()
    {
        movingTransform = transform.GetChild(0);
        movingTransform.position = transform.position;
        currentSpeed = new Vector3(0f, 0f, 0f);
        switch (way)
        {
            case whichWayToSlide.Horizontal:
                speed = new Vector3(Random.Range(minSpeed, maxSpeed), 0f, 0f);
                break;
            case whichWayToSlide.Vertical:
                speed = new Vector3(0f, Random.Range(minSpeed, maxSpeed), 0f);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (slideType == SlideType.SmoothChangeDirection)
        {
            if ((movingTransform.localPosition.x > bounds || movingTransform.localPosition.y > bounds) && moveDirection == 1)
            {
                currentSpeed = Vector3.Lerp(currentSpeed, new Vector3(0f, 0f, 0f), dampingFactor * Time.deltaTime);
            }
            else if ((movingTransform.localPosition.x < bounds * -1 || movingTransform.localPosition.y < bounds * -1) && moveDirection == -1)
            {
                currentSpeed = Vector3.Lerp(currentSpeed, new Vector3(0f, 0f, 0f), dampingFactor * Time.deltaTime);
            }
            else
            {
                currentSpeed = Vector3.Lerp(currentSpeed, speed * moveDirection, dampingFactor * Time.deltaTime);
            }
            if (Mathf.Abs(currentSpeed.x) + Mathf.Abs(currentSpeed.y) < 0.2)
            {
                moveDirection = moveDirection * -1;
                currentSpeed = Vector3.Lerp(currentSpeed, speed * moveDirection, dampingFactor * Time.deltaTime);
            }
            movingTransform.Translate(currentSpeed * Time.deltaTime);
        }
        else
        {
            if (moveHorizontaly)
            {
                MoveHorizontally(LeftMargin, RightMargin);
            }
            if (moveVerticaly)
            {
                MoveVertically(minHeight, maxHeight);
            }
        }
        
    }

    protected virtual void MoveHorizontally(float minPosition, float maxPosition)
    {
        if (movingTransform.localPosition.x > maxPosition)
            horizontalSign = -1;
        if (movingTransform.localPosition.x < minPosition)
            horizontalSign = 1;
        movingTransform.Translate(new Vector2(sliderSpeedH * horizontalSign * Time.deltaTime, 0));
    }

    protected virtual void MoveVertically(float minHeight, float maxHeight)
    {
        if (movingTransform.localPosition.y > maxHeight)
            verticalSign = -1;
        if (movingTransform.localPosition.y < minHeight)
            verticalSign = 1;
        movingTransform.Translate(new Vector2(0, sliderSpeedV * verticalSign * Time.deltaTime));
    }
}
