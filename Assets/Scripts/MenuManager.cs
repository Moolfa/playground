﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject levelSelctor;
    [SerializeField] GameObject inGameMenu;
    [SerializeField] GameObject gameTitle;
    [SerializeField] Button resumeButton;
    [SerializeField] UnityEngine.EventSystems.EventSystem eventSystem;

    private bool isGamePuased = false;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetButtonDown("Cancel"))
        {
            if (isGamePuased)
            {
                OnResumeButtonPress();
            }
            else
            {
                OnEscButtonPress();
            }

        }
    }

    public void OnEscButtonPress()
    {
        gameTitle.SetActive(false);
        inGameMenu.SetActive(true);
        resumeButton.Select();
        isGamePuased = true;
        Time.timeScale = 0f;
    }

    public void OnResumeButtonPress()
    {
        eventSystem.SetSelectedGameObject(null);
        gameTitle.SetActive(true);
        levelSelctor.SetActive(false);
        inGameMenu.SetActive(false);
        isGamePuased = false;
        Time.timeScale = 1f;
    }

    public void OnLevelsButtonPress()
    {
        eventSystem.SetSelectedGameObject(null);
        levelSelctor.SetActive(true);
    }

    public void OnExitButtonPress()
    {
        Application.Quit();
    }

}
