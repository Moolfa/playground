﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emerge : MonoBehaviour
{
    [SerializeField] private GameObject[] EmergedGameObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EmergePivot()
    {
        for (int i=0; i < EmergedGameObject.Length; i++)
        {
            EmergedGameObject[i].SetActive(true);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

    }
}
