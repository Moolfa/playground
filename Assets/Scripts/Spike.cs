﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spike : MonoBehaviour
{
    [SerializeField] bool reset = true;
    [SerializeField] Vector3 characterSpawnPosition = new Vector3(-10f, -5.58f, 0);

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        
    }

    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (reset)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            } else
            {
                collision.transform.position = characterSpawnPosition;
            }
            
        }
    }
}
