﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class LevelMenuManager : MonoBehaviour
{
    [SerializeField] private UnityEngine.EventSystems.EventSystem eventSystem;
    [SerializeField] private Scrollbar scrollbar;
    [SerializeField] private GameObject listContent;
    [SerializeField] private float getButtonDownInterval = 0.5f;
    [SerializeField] private float moveToItemDuration = 0.2f;
    [SerializeField] private float minimumSilderSpeed = 0.07f;

    private float distanceBetweenItems;
    private float sliderCurrentPosition;
    private float sliderPreviousPosition;
    private RectTransform selectedItem;
    private Coroutine listViewSlowDownRoutine;
    private int currentItem = 0;
    public int initialItem = 0;
    private int itemCount;

    private bool getButtonDown = true;
    private Coroutine buttonDownWaitCoroutine;


    // Start is called before the first frame update
    private void Awake()
    {
        InitialListView();
    }

    void OnEnable()
    {
        InitialPosition();
    }
    
    public void InitialListView()
    {
        itemCount = listContent.transform.childCount;
        float scrollViewHeight = GetComponent<RectTransform>().sizeDelta.y;
        float itemHeight = listContent.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;

        var parent = listContent.transform.parent;
        parent.GetChild(0).GetComponent<RectTransform>().sizeDelta =
            new Vector2(0f, (scrollViewHeight - itemHeight) / 2);
        parent.GetChild(2).GetComponent<RectTransform>().sizeDelta =
            new Vector2(0f, (scrollViewHeight - itemHeight) / 2);
        
            
            
        if (itemCount > 1)
        {
            distanceBetweenItems = 1.0f / (itemCount - 1);
        }
        else
        {
            distanceBetweenItems = 1.0f;
        }
        
        LayoutRebuilder.ForceRebuildLayoutImmediate(listContent.GetComponent<RectTransform>());
    }
    
    public void InitialPosition()
    {
        float target = 0f;
        if (initialItem >= 0 && initialItem <= itemCount)
        {
            target = initialItem * distanceBetweenItems;
            currentItem = initialItem;
        }
        MoveToTarget(target);
        RestoreSelectedItem(currentItem);
        StartCoroutine(DelayCall(0.1f, PlaySelectedItemAnimation));
    }
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow) && getButtonDown)
        {
            getButtonDown = false;
            if (buttonDownWaitCoroutine != null)
            {
                StopCoroutine(buttonDownWaitCoroutine);
            }
            buttonDownWaitCoroutine = StartCoroutine(GetButtonDownWaitCoroutine());
            OnNextItemClick();
            
        }

        if (Input.GetKey(KeyCode.UpArrow) && getButtonDown)
        {
            getButtonDown = false;
            if (buttonDownWaitCoroutine != null)
            {
                StopCoroutine(buttonDownWaitCoroutine);
            }
            buttonDownWaitCoroutine = StartCoroutine(GetButtonDownWaitCoroutine());
            OnPreviouseItemClick();
        }

        if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
        {
            getButtonDown = true;
            if (buttonDownWaitCoroutine != null)
            {
                StopCoroutine(buttonDownWaitCoroutine);
            }
        }

    }

    float CalculateTarget()
    {
        if (sliderCurrentPosition > 1)
        {
            return 0;
        }

        if (sliderCurrentPosition < 0)
        {
            return 1;
        }

        float itemOffset = sliderCurrentPosition % distanceBetweenItems;
        if (itemOffset < distanceBetweenItems / 2)
        {
            return 1 - (sliderCurrentPosition - itemOffset);
        }

        return 1 - (sliderCurrentPosition + distanceBetweenItems - itemOffset);
    }
    
    private void MoveToTarget(float target)
    {
        target = (1 - target);
        DOTween.To(() => scrollbar.value,
            (x) => scrollbar.value = x,
            target,
            moveToItemDuration).SetUpdate(true).PlayForward();
    }

    void RestoreSelectedItem(int itemIndex)
    {
        selectedItem = listContent.transform.GetChild(itemIndex).GetComponent<RectTransform>();
    }
    
    void PlaySelectedItemAnimation()
    {
        eventSystem.SetSelectedGameObject(selectedItem.GetChild(0).gameObject);
    }
    
    void PlayDeselectedItemAnimation()
    {
        eventSystem.SetSelectedGameObject(null);
    }
    
    

    public void OnScrollViewValueChanged(Vector2 value)
    {
        sliderCurrentPosition = value.y;
    }
    
    public void OnNextItemClick()
    {
        PlayDeselectedItemAnimation();
        
        currentItem += 1;
        if (currentItem >= itemCount)
        {
            currentItem = 0;
        }
        float target = currentItem * distanceBetweenItems;
        MoveToTarget(target);
        RestoreSelectedItem(currentItem);
        PlaySelectedItemAnimation();
    }

    public void OnPreviouseItemClick()
    {
        PlayDeselectedItemAnimation();
        currentItem -= 1;
        if (currentItem < 0)
        {
            currentItem = itemCount - 1;
        }
        float target = currentItem * distanceBetweenItems;
        MoveToTarget(target);
        RestoreSelectedItem(currentItem);
        PlaySelectedItemAnimation();
    }

    public void OnDragEnd()
    {
        if (listViewSlowDownRoutine != null)
        {
            StopCoroutine(listViewSlowDownRoutine);
        }

        listViewSlowDownRoutine = StartCoroutine(WaitForListViewSlowDown());
    }
    public void OnDragBegin()
    {
        PlayDeselectedItemAnimation();
        if (listViewSlowDownRoutine != null)
        {
            StopCoroutine(listViewSlowDownRoutine);
        }
    }

    public void OnSelectLevel(int index)
    {
        SceneManager.LoadScene(index);
        Time.timeScale = 1f;
    } 



    IEnumerator GetButtonDownWaitCoroutine()
    {
        yield return new WaitForSecondsRealtime(getButtonDownInterval);
        getButtonDown = true;
    }

    IEnumerator DelayCall(float delay, System.Action callBack)
    {
        yield return new WaitForSecondsRealtime(delay);
        callBack?.Invoke();
    }
    IEnumerator WaitForListViewSlowDown()
    {
        while (Mathf.Abs(sliderCurrentPosition - sliderPreviousPosition) > minimumSilderSpeed * Time.unscaledDeltaTime)
        {
            print(Mathf.Abs(sliderCurrentPosition - sliderPreviousPosition) + " / " +  (minimumSilderSpeed * Time.unscaledDeltaTime) + " -- " + Time.unscaledDeltaTime);
            sliderPreviousPosition = sliderCurrentPosition;
            yield return null;
        }
     
        float target = CalculateTarget();
        print(target);
        currentItem = Mathf.RoundToInt((target / distanceBetweenItems));
        RestoreSelectedItem(currentItem);
        MoveToTarget(target);
        PlaySelectedItemAnimation();
    }
}
